#include <platform.h>
#include <stdio.h>
#include "sdram.h"

on tile[0]: sdram_ports ports_0 = {
     XS1_PORT_16B , XS1_PORT_1J , XS1_PORT_1I , XS1_PORT_1K , XS1_PORT_1L , XS1_CLKBLK_1 };

on tile[2]: sdram_ports ports_1 = {
     XS1_PORT_16B , XS1_PORT_1J , XS1_PORT_1I , XS1_PORT_1K , XS1_PORT_1L , XS1_CLKBLK_1 };

/*
 * Chain together two core SliceKIT boards.
 * Plug one XA-SK-SDRAM into the TRIANGLE slot of the first board (tile 0).
 * Plug the second XA-SK-SDRAM into the TRIANGLE slot of the second board (tile 2).
 *
 * Build and run.
 */

void application(chanend c_server, char *name) {
#define BUF_WORDS (6)
  unsigned read_buffer[BUF_WORDS];
  unsigned write_buffer[BUF_WORDS];
  unsigned bank = 0, row = 0, col = 0;

  for(unsigned i=0;i<BUF_WORDS;i++){
    write_buffer[i] = i;
    read_buffer[i] = 0;
  }

  // Write the write_buffer out to SDRAM.
  sdram_buffer_write(c_server, bank, row, col, BUF_WORDS, write_buffer);

  //Wait until idle, i.e. the sdram had completed writing.
  sdram_wait_until_idle(c_server, write_buffer);

  // Read the SDRAM into the read_buffer.
  sdram_buffer_read(c_server, bank, row, col, BUF_WORDS, read_buffer);

  //Wait until idle, i.e. the sdram had completed reading and hence the data is ready in the buffer.
  sdram_wait_until_idle(c_server, read_buffer);

  for(unsigned i=0;i<BUF_WORDS;i++){
    printf("%s\t%08x\t%08x\n", name, write_buffer[i], read_buffer[i]);
    if(read_buffer[i] != i){
      printf("%s\tSDRAM demo fail.\n", name);
      sdram_shutdown(c_server);
      return;
    }
  }
  printf("%s\tSDRAM demo complete.\n", name);
  //Turn the SDRAM server off
  sdram_shutdown(c_server);
}

int main() {
  chan sdram_c, sdram_d;
  par {
    on tile[2]:sdram_server(sdram_c, ports_1);
    on tile[2]:application(sdram_c, "tile 2");
    on tile[0]:sdram_server(sdram_d, ports_0);
    on tile[0]:application(sdram_d, "tile 0");
  }


  return 0;
}

